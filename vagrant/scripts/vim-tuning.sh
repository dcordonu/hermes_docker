#! /bin/bash

echo "Customizing Vim..."

echo "set tabstop=4" >> /home/vagrant/.vimrc
echo "set shiftwidth=4" >> /home/vagrant/.vimrc
echo "set expandtab" >> /home/vagrant/.vimrc
