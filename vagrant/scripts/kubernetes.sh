#! /bin/bash

echo "Installing Kubernetes..."

echo "GRUB_CMDLINE_LINUX=\"cgroup_enable=memory\"" | tee -a /etc/default/grub
update-grub
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
chmod +x ./kubectl
mv ./kubectl /usr/local/bin/kubectl
echo "source <(kubectl completion bash)" >> /home/vagrant/.bashrc
curl -Lo minikube https://storage.googleapis.com/minikube/releases/v0.26.0/minikube-linux-amd64 && chmod +x minikube && mv minikube /usr/local/bin/

echo "export MINIKUBE_WANTUPDATENOTIFICATION=false" | tee -a /home/vagrant/.bashrc
echo "export MINIKUBE_WANTREPORTERRORPROMPT=false" | tee -a /home/vagrant/.bashrc
echo "export MINIKUBE_HOME=/home/vagrant" | tee -a /home/vagrant/.bashrc
echo "export CHANGE_MINIKUBE_NONE_USER=true" | tee -a /home/vagrant/.bashrc
mkdir -p /home/vagrant/.kube
touch /home/vagrant/.kube/config
echo "export KUBECONFIG=/home/vagrant/.kube/config" | tee -a /home/vagrant/.bashrc
chown -R vagrant:vagrant /home/vagrant/.kube
