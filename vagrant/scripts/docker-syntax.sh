#! /bin/bash

echo "Installing syntax highlighting for Vim..."

mkdir -p /home/vagrant/.vim/autoload /home/vagrant/.vim/bundle && curl -LSso /home/vagrant/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
echo "execute pathogen#infect()" > /home/vagrant/.vimrc
echo "syntax on" >> /home/vagrant/.vimrc
echo "filetype plugin indent on" >> /home/vagrant/.vimrc

git clone https://github.com/ekalinin/Dockerfile.vim.git /home/vagrant/.vim/bundle/docker.vim

echo "Installing syntax highlighting for Nano..."

git clone https://github.com/scopatz/nanorc.git /home/vagrant/.nano
cat /home/vagrant/.nano/nanorc >> /home/vagrant/.nanorc
