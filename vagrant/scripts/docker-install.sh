#! /bin/bash

echo "Installing Docker..."

apt-get -qqy install apt-transport-https ca-certificates curl gnupg2 software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
apt-get -qqy update
apt-get -qqy install docker-ce
usermod -aG docker vagrant
printf '{\n"insecure-registries" : ["docker-registry:5000"]}' > /etc/docker/daemon.json
