# coding=utf-8

import sys

import Utils

__author__ = 'Diego Cordón'

class Arguments:

    def __init__(self, project, rebuild=False, rebuild_images=False, detach=False, stop=False):
        self.project = project
        self.rebuild = rebuild
        self.rebuild_images = rebuild_images
        self.detach = detach
        self.stop = stop
