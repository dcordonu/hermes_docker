import shlex
import subprocess
import sys

from Constants import Commands, Paths

def stop_containers(project):
    try:
        subprocess.call(shlex.split(Commands.STOP_CONTAINERS_COMMAND), cwd=Paths.calculate_path(project))
    except KeyboardInterrupt:
        sys.exit()

def delete_containers(project):
    try:
        subprocess.call(shlex.split(Commands.REMOVE_CONTAINERS_COMMAND), cwd=Paths.calculate_path(project))
    except KeyboardInterrupt:
        sys.exit()

def start_containers(project, detached=False):
    command = Commands.LAUNCH_BACKGROUND_COMMAND if detached else Commands.LAUNCH_COMMAND
    try:
        subprocess.call(shlex.split(command), cwd=Paths.calculate_path(project))
    except KeyboardInterrupt:
        sys.exit()
