import shlex
import subprocess
import sys

from Constants import Commands, Paths

def delete_images(project):
    try:
        subprocess.call(shlex.split(Commands.REMOVE_IMAGES_COMMAND), cwd=Paths.calculate_path(project), shell=True)
    except KeyboardInterrupt:
        sys.exit()
