# coding=utf-8

import argparse
import os

from bean.Arguments import Arguments


def extract_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('project', help='Proyecto de Hermes que se quiere lanzar')
    parser.add_argument('-r', '--rm', default=False, action='store_true', help='Flag para forzar el borrado y reconstrucción de los contenedores')
    parser.add_argument('-i', '--rmi', default=False, action='store_true', help='Flag para forzar el borrado y reconstrucción de los contenedores y las imágenes')
    parser.add_argument('-d', '--detach', default=False, action='store_true', help='Flag para lanzar los servicios en background')
    parser.add_argument('-s', '--stop', default=False, action='store_true', help='Si está presente, se detienen los contenedores en vez de arrancarlos')

    args = parser.parse_args()

    return Arguments(
        project=args.project,
        rebuild=args.rmi or args.rm,
        rebuild_images=args.rmi,
        detach=args.detach,
        stop=args.stop
    )
