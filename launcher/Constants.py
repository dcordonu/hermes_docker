import os

class Paths:

    def __init__(self):
        pass

    @staticmethod
    def calculate_path(project):
        file_path = os.path.dirname(os.path.abspath(__file__))
        parent_folder = os.path.abspath(os.path.join(file_path, os.pardir))
        projects_folder = os.path.abspath(os.path.join(parent_folder, 'projects'))
        
        return os.path.abspath(os.path.join(projects_folder, project))

class Commands:

    def __init__(self):
        pass
    
    STOP_CONTAINERS_COMMAND = """docker-compose stop"""

    REMOVE_CONTAINERS_COMMAND = """docker-compose rm -f"""

    REMOVE_IMAGES_COMMAND = """docker-compose images -q | xargs docker rmi -f"""

    LAUNCH_COMMAND = """docker-compose up"""

    LAUNCH_BACKGROUND_COMMAND = LAUNCH_COMMAND + ' -d'
