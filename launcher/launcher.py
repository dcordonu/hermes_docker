#! /usr/bin/python
# coding=utf-8

import Utils

from task import Container, Image

__author__ = 'Diego Cordón'

arguments = Utils.extract_args()

if arguments.rebuild_images:
    Container.stop_containers(arguments.project)

if arguments.rebuild:
    Container.delete_containers(arguments.project)
    Image.delete_images(arguments.project)

if arguments.stop:
    Container.stop_containers(arguments.project)
else:
    Container.start_containers(arguments.project)
